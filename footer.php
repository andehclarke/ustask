<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
					
				<footer class="footer us-footer">
					
					<div class="inner-footer grid-x grid-margin-x grid-padding-x us-footer-black">
						
						<div class="small-12 medium-4 large-4 cell">
							<nav>
	    						<?php joints_footer_links(); ?>
	    					</nav>
	    				</div>
						
						<div class="small-12 medium-4 large-4 cell text-center">
							<p class="source-org copyright"><?php bloginfo('name'); ?> &copy; Copyright <?php echo date('Y'); ?></p>
						</div>

						<div class="cell small-12 medium-4 large-4">
							<p></p>
						</div>
					
					</div> <!-- end #inner-footer -->
				
				</footer> <!-- end .footer -->
			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->
		
		<?php wp_footer(); ?>
		
	</body>
	
</html> <!-- end page -->
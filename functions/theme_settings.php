<?php

// Add custom theme settings page
function theme_settings_page(){
    ?>
    <div class="wrap">
        <h1>Theme Panel</h1>
        <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
    </div>
	<?php
}

// Add it to the admin menu
function add_theme_menu_item()
{
    add_menu_page("Theme Settings", "Theme Settings", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

function display_contact_email_element()
{
	?>
    	<input type="text" name="contact_email_address" id="contact_email_address" value="<?php echo get_option('contact_email_address'); ?>" />
    <?php
}

function display_contact_phone_element()
{
	?>
    	<input type="text" name="contact_phone_number" id="contact_phone_number" value="<?php echo get_option('contact_phone_number'); ?>" />
    <?php
}

// Add custom settings to view and register them
function display_theme_panel_fields()
{
    add_settings_section("section", "All Settings", null, "theme-options");
	add_settings_field("contact_email_address", "Email address", "display_contact_email_element", "theme-options", "section");
    add_settings_field("contact_phone_number", "Phone number", "display_contact_phone_element", "theme-options", "section");
    register_setting("section", "contact_email_address");
    register_setting("section", "contact_phone_number");
}

add_action("admin_init", "display_theme_panel_fields");
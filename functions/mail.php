<?php
function contact_email() {
    if(isset($_POST['anti-spam']) && $_POST['anti-spam'] == ''){
        $to = "hello@andyclarke.website";
        // $to = get_option('contact_email');
        $reply_to = $_POST['cf_email'];
        $from = $reply_to;
        $body = wordwrap($_POST['cf_message'], 70, "\r\n");
        $name = $_POST['cf_name'];
        $subject = "Contact form";
        $headers = 'From: '.$from."\r\n".'Reply-To: '.$reply_to;
        // then send the form to your email
        wp_mail($to, $subject, $body, $headers);
    }
}
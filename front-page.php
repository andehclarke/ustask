<?php 
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 */

get_header(); ?>

<!-- Contact email function -->

<?php contact_email(); ?>
	
	<div class="content">
	
		<div class="inner-content">
	
		    <main class="main grid-x grid-margin-x">

				<div class="orbit us-orbit cell small-12" role="region" aria-label="Leicester groundsworks services pictures" data-orbit>
					<div class="orbit-wrapper">
						<div class="orbit-controls">
							<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span></button>
							<button class="orbit-next"><span class="show-for-sr">Next Slide</span></button>
						</div>
						<span class="slider-bullet"></span>
						<ul class="orbit-container">
						<li class="is-active orbit-slide">
							<figure class="orbit-figure">
							<img class="orbit-image" src="<?php echo get_template_directory_uri(); ?>/assets/images/slider-1.png" alt="Image of a building site" />
							<img class="slider-shadow slider-shadow-overlay" src="<?php echo get_template_directory_uri(); ?>/assets/images/slider-shadow.png" alt="" />
					<img class="slider-shadow slider-shadow-bottom" src="<?php echo get_template_directory_uri(); ?>/assets/images/slider-shadow-bottom.png" alt="" />
							<figcaption class="orbit-caption"><h1>landscaping, civil engineering and groundworks contractors</h1></figcaption>
							</figure>
						</li>
						<li class="orbit-slide">
							<figure class="orbit-figure">
							<img class="orbit-image" src="<?php echo get_template_directory_uri(); ?>/assets/images/slider-1.png" alt="Image of a building site" />
							<img class="slider-shadow slider-shadow-overlay" src="<?php echo get_template_directory_uri(); ?>/assets/images/slider-shadow.png" alt="" />
					<img class="slider-shadow slider-shadow-bottom" src="<?php echo get_template_directory_uri(); ?>/assets/images/slider-shadow-bottom.png" alt="" />
							<figcaption class="orbit-caption"><h1>slide 2</h1></figcaption>
							</figure>
						</li>
						</ul>
					</div>
					<nav class="orbit-bullets">
						<button class="is-active" data-slide="0"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
						<button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
					</nav>
					
				</div>

				<?php 
					if (isset($_GET['success'])) {
						echo '<div class="callout success cell small-12 medium-6 medium-offset-3" data-closable="slide-out-right">
						<h5>Message sent</h5>
						<p>Thank\'s for sending us your message. We\'ll be in touch shortly.</p>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
						<span aria-hidden="true">&times;</span>
						</button>
						</div>';
					} 
				?>

				<div class="us-main-content cell small-10 small-offset-1 medium-10 medium-offset-1 large-8 large-offset-2">

					<div id="location-map" class="us-map">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/map.png" alt="Map of locations" />
					</div>

					<div class="us-page-content">
					
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php // get_template_part( 'parts/loop', 'page' ); ?>
						
						<?php endwhile; endif; ?>

						<div class="grid-x grid-margin-x">

							<div class="card cell small-6 medium-3">

								<?php if ( is_active_sidebar( 'service1' ) ) :
									
										dynamic_sidebar( 'service1' );
									
									else : 
										
										echo '<div class="card-image"><img src="'.get_template_directory_uri().'/assets/images/pipe.png" alt="Image of workmen around a pipe" /></div><div class="card-button"><a href="" class="button expanded">Drainage<span class="us-chev">&raquo;</span></a></div>'; 
								
									endif; 
								
								?>

							</div>

							<div class="card cell small-6 medium-3">

								<?php if ( is_active_sidebar( 'service2' ) ) :
								
										dynamic_sidebar( 'service2' );
									
									else : 
										
										echo '<div class="card-image"><img src="'.get_template_directory_uri().'/assets/images/dig.png" alt="Image of workmen repairing a pothole" /></div><div class="card-button"><a href="" class="button expanded">Resurfacing<span class="us-chev">&raquo;</span></a></div>'; 
								
									endif; 
									
								?>

							</div>

							<div class="card cell small-6 medium-3">

								<?php if ( is_active_sidebar( 'service3' ) ) :
									
										dynamic_sidebar( 'service3' );
								
									else : 
										
										echo '<div class="card-image"><img src="'.get_template_directory_uri().'/assets/images/digger.png" alt="Image of a JCB digger" /></div><div class="card-button"><a href="" class="button expanded">Vehicle Hire<span class="us-chev">&raquo;</span></a></div>'; 
								
									endif; 
									
								?>
							
							</div>

							<div class="card cell small-6 medium-3">

								<?php if ( is_active_sidebar( 'service4' ) ) :
									
										dynamic_sidebar( 'service4' );
									
									else : 
										
										echo '<div class="card-image"><img src="'.get_template_directory_uri().'/assets/images/camera.png" alt="Image of a mapping camera" /></div><div class="card-button"><a href="" class="button expanded">Mapping<span class="us-chev">&raquo;</span></a></div>'; 
								
									endif; 
									
								?>
								
							</div>

						</div>
					
					</div>

					<div id="us-contact">
						<form data-abide novalidate action="?success" method="POST" id="us-contact-form">
							<div data-abide-error class="alert callout" style="display: none;">
								<p><i class="fi-alert"></i> There are some errors in your form.</p>
							</div>
							<div class="grid-x grid-margin-x row">
								<div class="cell small-12 medium-6 columns">
								<label>
									Name
									<input type="text" name="cf_name" placeholder="Your Name" required pattern="text" />
									<span class="form-error">
										What's your name?
									</span>
								</label>
								</div>
								<div class="cell small-12 medium-6 columns">
								<label>
									Email
									<input type="email" name="cf_email" placeholder="example@email.com" required pattern="email" />
									<span class="form-error">
										What's your email?
									</span>
								</label>
								</div>
							</div>
							<div class="row grid-x grid-margin-x">
								<div class="cell small-12 columns">
								<label>
									Message
									<textarea rows="5" name="cf_message" placeholder="Your message for us"></textarea>
								</label>
								</div>
							</div>
							<div class="row grid-x grid-margin-x submit">
								<input class="hide small-12" name="anti-spam" type="text" />
								<button class="button cell small-12 medium-3 columns" type="submit" value="Send">Submit<span class="us-chev">&raquo;</span></button>
							</div>
						</form>
					</div>

				</div>
			    					
			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
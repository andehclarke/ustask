<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="top-bar">
	<div class="top-bar-left us-logo float-left">
		<a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="<?php bloginfo('name'); ?>"></a>
	</div>
	<div class="top-bar-right us-container">
		<?php
			$contact_email = get_option('contact_email_address');
			$contact_phone = get_option('contact_phone_number');
			if($contact_phone != null) {
				echo '<h2>'.$contact_phone.'</h2>';
			}
			if($contact_email != null) {
				echo '<h3><a href="mailto:'.$contact_email.'">'.$contact_email.'</a></h3>';
			}
		?>
	</div>
</div>
<div class="cell small-12 medium-8 medium-offset-2 us-nav show-for-medium">
	<?php
		joints_top_nav();
	?>
</div>
<div class="cell show-for-small-only">
	<ul class="menu align-center">
		<!-- <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> -->
		<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
	</ul>
</div>
